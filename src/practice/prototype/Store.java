package practice.prototype;

public class Store {

    private String name;

    private String country;

    private String city;

    private String category;

    public Store() {}

    public Store(String name, String country, String city, String category) {
        this.name = name;
        this.country = country;
        this.city = city;
        this.category = category;
    }

    public Store(Store store) {
        this.name = store.name;
        this.country = store.country;
        this.city = store.city;
        this.category = store.category;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Store clone() {
        return new Store(this.name, this.country, this.city, this.category);
    }
}
