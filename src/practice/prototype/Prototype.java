package practice.prototype;

import java.util.Collections;

public class Prototype {

    public static void main(String[] args) {
        Store s = new Store("Toko A", "Indonesia", "Tangerang", "fashion");
        Store s2 = s.clone();
        s2.setName("Toko B");

        //Store s3 = s; // <-- this method s and s3 are same
        Store s3 = new Store(s);
        s3.setName("Toko C");

        System.out.println(s.getName());
        System.out.println(s2.getName());
        System.out.println(s3.getName());
    }
}
