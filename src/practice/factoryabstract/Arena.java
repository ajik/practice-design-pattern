package practice.factoryabstract;

public class Arena {

    private String arena;

    public String getArena() {
        return arena;
    }

    public void setArena(String arena) {
        this.arena = arena;
    }

    public void start() {
        System.out.println("Level Arena: " + this.arena);
    }
}
