package practice.factoryabstract;

public class Game {

    private Level level;
    private Arena arena;

    public Game(GameFactory gf) {
        this.level = gf.createLevel();
        this.arena = gf.createArena();
    }

    public void Start() {
        this.level.start();
        this.arena.start();
    }
}
