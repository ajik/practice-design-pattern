package practice.factoryabstract;

public interface GameFactory {
    public Level createLevel();
    public Arena createArena();
}

class GameFactoryEasy implements GameFactory {

    @Override
    public Level createLevel() {
        Level l = new Level();
        l.setLevel("Easy");
        return l;
    }

    @Override
    public Arena createArena() {
        Arena a = new Arena();
        a.setArena("Savanna");
        return a;
    }
}

class GameFactoryNormal implements GameFactory {

    @Override
    public Level createLevel() {
        Level l = new Level();
        l.setLevel("Normal");
        return l;
    }

    @Override
    public Arena createArena() {
        Arena a = new Arena();
        a.setArena("Desert");
        return a;
    }
}

class GameFactoryHard implements GameFactory {

    @Override
    public Level createLevel() {
        Level l = new Level();
        l.setLevel("hard");
        return l;
    }

    @Override
    public Arena createArena() {
        Arena a = new Arena();
        a.setArena("Amazon Jungle");
        return a;
    }
}

class GameFactoryExtreme implements GameFactory {

    @Override
    public Level createLevel() {
        Level l = new Level();
        l.setLevel("extreme");
        return l;
    }

    @Override
    public Arena createArena() {
        Arena a = new Arena();
        a.setArena("South Pole (Blizzard)");
        return a;
    }
}