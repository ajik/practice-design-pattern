package practice.factoryabstract;

public class Level {

    private String level;

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public void start() {
        System.out.println("Level: " + this.level);
    }
}
