package practice.factoryabstract;

public class FactoryAbstract {

    public static void main(String[] args) {
        Game gameEasy = new Game(new GameFactoryEasy());
        Game gameNormal = new Game(new GameFactoryNormal());
        Game gamehard = new Game(new GameFactoryHard());
        Game gameExtreme = new Game(new GameFactoryExtreme());

        gameEasy.Start();
        gameNormal.Start();
        gamehard.Start();
        gameExtreme.Start();
    }
}
