package practice.template;

public class BlockGame2 extends BlockGameTemplate{

    @Override
    public String getStartTitle() {
        return "START BLOCK GAME 2";
    }

    @Override
    public String getEndTitle() {
        return "FINISH BLOCK GAME 2";
    }

    @Override
    public String getCharacter() {
        return "X";
    }

    @Override
    public int getSize() {
        return 5;
    }
}
