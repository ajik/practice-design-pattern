package practice.template;

public class BlockGame1 extends BlockGameTemplate{

    @Override
    public String getStartTitle() {
        return "START BLOCK GAME 1";
    }

    @Override
    public String getEndTitle() {
        return "FINISH BLOCK GAME 1";
    }

    @Override
    public String getCharacter() {
        return "0";
    }

    @Override
    public int getSize() {
        return 10;
    }
}
