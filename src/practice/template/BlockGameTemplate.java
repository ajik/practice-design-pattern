package practice.template;

public abstract class BlockGameTemplate {

    public final void start() {
        System.out.println(getStartTitle());

        for(int i = 0; i < getSize(); i++) {
            for(int j = 0; j < getSize(); j++) {
                System.out.print(getCharacter());
            }
            System.out.println();
        }

        System.out.println(getEndTitle());
    }

    public abstract String getStartTitle();

    public abstract String getEndTitle();

    public abstract String getCharacter();

    public abstract int getSize();
}
