package practice.adapter;

public class ScreencastCatalogAdapter implements CatalogAdapter{

    private ScreenCast screencast;

    public ScreencastCatalogAdapter(ScreenCast screencast) {
        this.screencast = screencast;
    }

    @Override
    public String getCatalogTitle() {
        return screencast.getTitle() + ", By "+ screencast.getAuthor();
    }
}
