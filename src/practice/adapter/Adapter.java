package practice.adapter;

import java.util.ArrayList;
import java.util.List;

public class Adapter {
    public static void main(String[] args) {
        //List<Object> list = new ArrayList<>();
        List<CatalogAdapter> list = new ArrayList<>();

        list.add(new BookCatalogAdapter(new Book("Buku 1", "Edi")));
        list.add(new BookCatalogAdapter(new Book("Buku 2", "Ani")));
        list.add(new BookCatalogAdapter(new Book("Buku 3", "Djono")));

        list.add(new ScreencastCatalogAdapter(new ScreenCast("Screencast 1", "Djoni", 10000L)));
        list.add(new ScreencastCatalogAdapter(new ScreenCast("Screencast 2", "Djono", 10000L)));
        list.add(new ScreencastCatalogAdapter(new ScreenCast("Screencast 3", "Djini", 10000L)));

        /*list.forEach(item -> {
            if(item instanceof Book) {
                Book book = (Book) item;
                System.out.println(book.getTitle() + ", By "+ book.getAuthor());
            } else if(item instanceof ScreenCast) {
                ScreenCast sc = (ScreenCast) item;
                System.out.println(sc.getTitle() + ", By " + sc.getAuthor());
            }
        });*/

        list.forEach(i -> {
            System.out.println(i.getCatalogTitle());
        });
    }
}
