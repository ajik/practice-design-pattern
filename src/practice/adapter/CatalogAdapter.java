package practice.adapter;

public interface CatalogAdapter {

    /**
     * Convert interface to shape that can be read generally
     *
     * @return
     */
    public String getCatalogTitle();
}
