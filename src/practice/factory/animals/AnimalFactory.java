package practice.factory.animals;

public class AnimalFactory {

    public static Animal create(String type) {
        if(type.equalsIgnoreCase("cat")) {
            return new Cat();
        } else if(type.equalsIgnoreCase("dog")){
            return new Dog();
        } else if(type.equalsIgnoreCase("spider")) {
            return new Spider2Evolution();
        }
        return null;
    }
}
