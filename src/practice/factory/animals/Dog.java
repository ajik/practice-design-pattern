package practice.factory.animals;

public class Dog implements Animal{

    @Override
    public String speak() {
        return "WOFF";
    }

    @Override
    public int totalLeg() {
        return 4;
    }

    @Override
    public String beatEnemy() {
        return "BITE";
    }
}
