package practice.factory.animals;

@Deprecated
public class Spider implements Animal{

    @Override
    public String speak() {
        return "";
    }

    @Override
    public int totalLeg() {
        return 8;
    }

    @Override
    public String beatEnemy() {
        return "TRAP IN WEB";
    }
}
