package practice.factory.animals;

public class Spider2Evolution implements Animal{

    @Override
    public String speak() {
        return "SHHSSHHHH";
    }

    @Override
    public int totalLeg() {
        return 4;
    }

    @Override
    public String beatEnemy() {
        return "BITE TO DIE";
    }
}
