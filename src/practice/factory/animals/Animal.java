package practice.factory.animals;

public interface Animal {

    public String speak();
    public int totalLeg();
    public String beatEnemy();
}
