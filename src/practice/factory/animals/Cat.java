package practice.factory.animals;

public class Cat implements Animal{

    @Override
    public String speak() {
        return "nyaa";
    }

    @Override
    public int totalLeg() {
        return 4;
    }

    @Override
    public String beatEnemy() {
        return "CLAW";
    }
}
