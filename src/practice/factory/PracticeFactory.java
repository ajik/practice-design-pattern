package practice.factory;

import practice.factory.animals.Animal;
import practice.factory.animals.AnimalFactory;

public class PracticeFactory {

    public static void main(String[] args) {
        Animal a = AnimalFactory.create("dog");
        System.out.println(a.beatEnemy());
    }


}
