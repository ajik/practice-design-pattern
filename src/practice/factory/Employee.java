package practice.factory;

public class Employee {

    private String name;
    private String nip;
    private String alamat;

    public Employee(String name, String nip, String alamat) {
        this.name = name;
        this.nip = nip;
        this.alamat = alamat;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNip() {
        return nip;
    }

    public void setNip(String nip) {
        this.nip = nip;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }
}
