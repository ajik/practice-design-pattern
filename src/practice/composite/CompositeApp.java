package practice.composite;

import java.util.Arrays;
import java.util.List;

public class CompositeApp {

    public static void main(String[] args) {
        CompositeApp ca = new CompositeApp();
        List<Category> categories = Arrays.asList(
                new CategoryComposite("Buku", Arrays.asList(
                        new Category("Majalah"),
                        new Category("Buku Sekolah")
                )),
                new CategoryComposite("Pakaian", Arrays.asList(
                        new Category("Baju Wanita"),
                        new CategoryComposite("Baju Pria", Arrays.asList(
                                new CategoryComposite("Kemeja", Arrays.asList(
                                        new Category("Kemeja size-fit"),
                                        new Category("Kemeja Lengan Panjang")
                                )),
                                new Category("Kaos Oblong")
                        ))
                )),
                new CategoryComposite("Alat Elektronik", Arrays.asList(
                        new Category("Laptop"),
                        new Category("Smartphone")
                )),
                new Category("Pemabayaran PLN")
        );

        categories.forEach(i-> {
            ca.printCategory(i);
        });
    }

    private void printCategory(Category category) {
        System.out.println(category.getName());
        if(category instanceof CategoryComposite) {
            CategoryComposite comp = (CategoryComposite) category;
            comp.getCategoryList().forEach(i->{
                printCategory(i);
            });
        }
    }
}
