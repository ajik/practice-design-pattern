package practice.composite;

import java.util.ArrayList;
import java.util.List;

public class CategoryComposite extends Category{

    private List<Category> categoryList = new ArrayList<>();

    public CategoryComposite(String name, List<Category> categoryList) {
        super(name);
        this.categoryList = categoryList;
    }

    public List<Category> getCategoryList() {
        return categoryList;
    }

    public void setCategoryList(List<Category> categoryList) {
        this.categoryList = categoryList;
    }
}
