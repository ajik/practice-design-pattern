package practice.bridge;

public class Kuda extends BinatangDarat {

    @Override
    public String getNama() {
        return "KUDA";
    }

    public Integer getKaki() {
        return 4;
    }
}
