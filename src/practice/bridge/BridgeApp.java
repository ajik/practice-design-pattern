package practice.bridge;

import java.util.Arrays;
import java.util.List;

public class BridgeApp {

    public static void main(String[] args) {
        List<Binatang> binatangs = Arrays.asList(
                new Buaya(),
                new Ikan(),
                new Kucing(),
                new Kuda()
        );

        binatangs.forEach(binatang -> {
            if(binatang instanceof BinatangDarat) {
                BinatangDarat bd = (BinatangDarat) binatang;
                System.out.println(bd.getNama() + "Adalah binatang darat, jumlah kaki: "+ bd.getKaki());
            } else if(binatang instanceof BinatangAir) {
                BinatangAir ba = (BinatangAir) binatang;
                System.out.println(ba.getNama() + "Adalah binatang air");
            }
        });
    }
}
