package practice.bridge;

public interface Binatang {

    String getNama();

    boolean hidupDiDarat();

    boolean hidupDiAir();
}
