package practice.bridge;

public class Kucing extends BinatangDarat {

    @Override
    public String getNama() {
        return "KUCING";
    }

    public Integer getKaki() {
        return 4;
    }
}
